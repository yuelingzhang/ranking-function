(declare-rel inv (Int) )
(declare-var x Int)
(declare-var x1 Int)


(declare-rel fail ())

(rule (inv x))
(rule 
    (=>
        (and 
			(inv x)
			(>= 0 x)
			(or 
				(= x1 (+ x 1) )
				(= x1 (+ x 2) )
				(= x1 (+ x 3) )
				(= x1 (+ x 4) )
				(= x1 -1 )
			)
        )  
        (inv x1 )
    )
)

(rule
    (=>
        (and
            (inv x)
			(>= 0 x)
        )
        fail
    )
)

(query fail :print-certificate true)

