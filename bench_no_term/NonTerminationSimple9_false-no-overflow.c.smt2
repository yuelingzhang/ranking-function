(declare-rel inv (Int) )
(declare-var x Int)
(declare-var x1 Int)


(declare-rel fail ())

(rule (inv x))
(rule 
    (=>
        (and 
			(inv x)
			(>= 0 x)
        )  
        (inv x1 )
    )
)

(rule
    (=>
        (and
            (inv x)
			(>= 0 x)
        )
        fail
    )
)

(query fail :print-certificate true)

