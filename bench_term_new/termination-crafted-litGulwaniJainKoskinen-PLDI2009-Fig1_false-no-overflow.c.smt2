(declare-rel inv (Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var z Int)
(declare-var z1 Int)

(rule (=> 
	(and 
		(<= 0 x)
		(< x y)
		(= z (+ x 1))
	)
	(inv x y z))
)

(rule (=> 
    (and 
        (inv x y z)
        (not (= z x))
		(or 
			(and 
				(<= z y)
				(= z1 (+ z 1))
				(= y1 y)
				(= x1 x)
			)
			(and 
				(not (<= z y))
				(= z1 0)
				(= y1 y)
				(= x1 x)
			)
		)
    )
    (inv x1 y1 z1)
  )
)


