(declare-rel inv (Int Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var a Int)
(declare-var a1 Int)
(declare-var b Int)
(declare-var b1 Int)
(declare-var t Int)
(declare-var t1 Int)
(rule (=> 
	(and 
		(>= t 0)
		(= a (* 2 t))
		(= b (* 2 t))
	)
	(inv x a b t))
)

(rule (=> 
    (and 
        (inv x a b t)
        (>= a t)
		(or 
			(and 
				(= a1 (- a 1))
				(= x1 x)
				(= b1 b)
				(= t1 t)
			)
			(and 
				(= t1 a)
				(= a1 (- b 1))
				(= b1 a)
				(= x1 x)
			)
		)
    )
    (inv x1 a1 b1 t1)
  )
)


