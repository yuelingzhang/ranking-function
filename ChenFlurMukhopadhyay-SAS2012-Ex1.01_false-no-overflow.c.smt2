(declare-rel inv (Int Int ))
(declare-var x Int)
(declare-var x1 Int)
(declare-var gh Int)
(declare-var gh1 Int)

(declare-rel err ())

(rule 
  (=> (> x 0) (inv x gh) 
  )    
)

(rule
  (=> (and (inv x gh)
        (> x 0) 
        (= gh1 x)
        (= x1 (- 10 (* 2 x)))
      )
  (inv x1 gh1))
)

(rule (=> 
        (and (inv x gh)
          (> x 0) 
          (not 
            (>= gh 0)
          )
        )
        err
      )
)
(query err)