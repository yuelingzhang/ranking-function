(declare-rel inv (Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var n Int)
(declare-var n1 Int)
(rule (=> 
	(and 
		(< n 536870912)
		(< x 536870912)
		(< y 536870912)
		(>= x -1073741824)
		(>= (+ x y) 0)
	)
	(inv x y n))
)

(rule (=> 
    (and 
        (inv x y n)
        (<= x n)
		(or 
			(and 
				(= x1 (+ (* 2 x) y))
				(= y1 (+ y 1 ))
				(= n1 n)
			)
			(and 
				(= x1 (+ x 1))
				(= y1 y)
				(= n1 n)
			)
		)
    )
    (inv x1 y1 n1)
  )
)


