(declare-rel inv (Int Int Int) )
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var oldx Int)
(declare-var oldx1 Int)

(declare-rel fail ())

(rule (inv x y oldx ) )

(rule 
    (=>
        (and 
			(inv x y oldx )
			(> (+ (* 4 x) y) 0)
			(= oldx1 x)
            (= x1 (+ (* -2 oldx) (* 4 y) ) )
            (= y1 (* 4 oldx))
        )  
        (inv x1 y1 oldx1 )
    )
)

(rule
    (=>
        (and
            (inv x y oldx )
			(> (+ (* 4 x) y) 0)
        )
        fail
    )
)

(query fail :print-certificate true)

