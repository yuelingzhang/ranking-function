(declare-rel inv (Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(rule (=> 
	(and 
		(= x 0)
		(= y 100)
	)
	(inv x y ))
)

(rule (=> 
    (and 
        (inv x y  )
        (< x y)
		(or 
			(and 
				(< 51 y)
				(= x1 (+ x 1))
				(= y1 (- y 1))
			)
			(and 
				(not (< 51 y))
				(= x1 (- x 1))
				(= y1 (+ y 1))
			)
		)
    )
    (inv x1 y1  )
  )
)


