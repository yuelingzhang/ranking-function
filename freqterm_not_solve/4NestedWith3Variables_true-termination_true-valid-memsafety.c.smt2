(declare-rel inv (Int Int Int Int) )
(declare-var a Int)
(declare-var a1 Int)
(declare-var b Int)
(declare-var b1 Int)
(declare-var q Int)
(declare-var q1 Int)
(declare-var olda Int)
(declare-var olda1 Int)

(declare-rel fail ())

(rule 
	(=> 
		(and 
			(<= -524287 q)
			(>= q 524287)
			(<= -524287 a)
			(>= a 524287)
			(<= -524287 b)
			(>= b 524287)
		)
		(inv a b q olda)
	)
)
(rule 
    (=>
        (and 
            (inv a b q olda)
            (> q 0)
            (= q1 (+ q (- a 1)))
            (= olda1 a)
            (= a1 (- (* 3 olda1) (* 4 b) ))
            (= b1 (+ (* 4 olda1) (* 3 b) ))
        )  
        (inv a1 b1 q1 olda1)
    )
)

(rule
    (=>
        (and
            (inv a b q olda)
            (> q 0)
        )
        fail
    )
)

(query fail :print-certificate true)
