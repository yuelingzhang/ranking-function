(declare-rel inv (Int ) )
(declare-var y Int)
(declare-var y1 Int)

(declare-rel fail ())

(rule (inv y) )
(rule 
    (=>
        (and 
			(inv y)
			(> y 1)
			(or 
				(and 
					(= 0 (mod y 2) )
					(= y1 (div y 2) )
				)
				(and 
					(not (= 0 (mod y 2) ) )
					(= y1 (+ (* 3 y) 1) )
				)
			)
        )  
        (inv y1)
    )
)

(rule
    (=>
        (and
            (inv y)
			(> y 1)
        )
        fail
    )
)

(query fail :print-certificate true)

