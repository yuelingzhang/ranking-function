(declare-rel inv (Int Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var z Int)
(declare-var z1 Int)
(declare-var r Int)
(declare-var r1 Int)
(rule (=> 
	(and 
		(= x 1)
		(= y 1)
		(= z 1)
		(= r 1000)
	)
	(inv x y z r))
)

(rule (=> 
    (and 
        (inv x y z r )
        (not (= (* x (* x x)) (+ (* y (* y y)) (* z (* z z)) )))
		(<= z r)
		(= x1 (+ x 1))
		(or 
			(and 
				(> x r)
				(= x1 1)
				(= y1 (+ y 1))
			)
			(and 
				(> y r)
				(= y1 1)
				(= z1 (+ z 1))
			)
		)
	)
    (inv x1 y1 z1 r1 )
  )
)


