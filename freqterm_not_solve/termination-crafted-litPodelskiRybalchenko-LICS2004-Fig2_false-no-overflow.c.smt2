(declare-rel inv (Int Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var ox Int)
(declare-var ox1 Int)
(declare-var oy Int)
(declare-var oy1 Int)
(rule 

	(inv x y ox oy)
)

(rule (=> 
    (and 
        (inv x y ox oy)
        (> x 0)
		(> y 0)
		(= ox1 x)
		(= oy1 y)
		(or 
			(and 
				(= x1 (- ox 1))
				(= y1 ox)
			)
			(and 
				(= x1 (- oy 2))
				(= y1 (+ ox 1))
			)
		)
    )
    (inv x1 y1 ox1 oy1)
  )
)


