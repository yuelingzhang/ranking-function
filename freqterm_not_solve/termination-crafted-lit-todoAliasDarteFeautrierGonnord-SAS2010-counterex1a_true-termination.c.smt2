(declare-rel inv (Int Int Int Int))
(declare-var n Int)
(declare-var n1 Int)
(declare-var b Int)
(declare-var b1 Int)
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(rule 
	(inv n b x y)
)

(rule (=> 
    (and 
        (inv n b x y)
        (>= x 0)
		(<= 0 y)
		(<= y n)
		(or 
			(and 
				(= b 0)
				(= y1 (+ y 1))
				(= n1 n)
				(= x1 x)
				(or 
					(= b1 b)
					(= b1 1)
				)
			)
			(and 
				(not (= b 0))
				(= y1 y)
				(= n1 n)
				(or 
					(= x1 (- x 1))
					(= b1 0)
				)
			)
		)
    )
    (inv n1 b1 x1 y1)
  )
)


