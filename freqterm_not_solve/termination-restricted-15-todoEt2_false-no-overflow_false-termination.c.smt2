(declare-rel inv (Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var y Int)
(declare-var y1 Int)
(declare-var r Int)
(declare-var r1 Int)
(rule 
	(inv x y r)
)

(rule (=> 
    (and 
        (inv x y r )
        (> y 0)
		(= y1 (- (- x 1) r))
		(= x1 (- (- x 1) r))
    )
    (inv x1 y1 r1 )
  )
)


