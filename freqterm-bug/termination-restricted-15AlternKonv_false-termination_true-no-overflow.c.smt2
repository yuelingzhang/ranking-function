(declare-rel inv (Int ))
(declare-var i Int)
(declare-var i1 Int)
(rule 
	(inv i )
)

(rule (=> 
    (and 
        (inv i  )
        (not (= i 0))
		(or 
			(and 
				(< i 0)
				(= i1 (+ i 2))
				(< i 0)
				(not (<= i -2147483648))
				(= i1 (* i -1))
			)
			(and 
				(or 
					(= i1 (- i 2))
					(and 
						(> i 2)
						(= i1 (- i 2))
						(= i1 (* i -1))
					)
				)
			)
		)
		
    )
    (inv i1  )
  )
)


