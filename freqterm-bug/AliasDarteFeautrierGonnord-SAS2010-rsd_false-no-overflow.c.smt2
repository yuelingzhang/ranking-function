(declare-rel inv (Int Int Int Int) )
(declare-var r Int)
(declare-var r1 Int)
(declare-var da Int)
(declare-var da1 Int)
(declare-var db Int)
(declare-var db1 Int)
(declare-var temp Int)
(declare-var temp1 Int)

(declare-rel fail ())

(rule 
    (=> 
        (and 
            (= da (* 2 r) )
            (= db (* 2 r) )
            (>= r 0)
        )
        (inv r da db temp )
    ) 
)
(rule 
    (=>
        (and 
			(inv r da db temp)
            (>= da r)
			(or 
				(and 
					(= da1 (- da 1)) (= db1 db) (= temp1 temp)
				)
				(and 
					(= temp1 da )
                    (= da1 (- db 1) )
                    (= db1 da)
				)
			)
        )  
        (inv r1 da1 db1 temp1 )
    )
)

(rule
    (=>
        (and
            (inv r da db temp )
			(>= da r)
        )
        fail
    )
)

(query fail :print-certificate true)

