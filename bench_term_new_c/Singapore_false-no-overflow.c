/*
 * Date: 06/07/2015
 * Created by: Ton Chanh Le (chanhle@comp.nus.edu.sg)
 */

typedef enum {false, true} bool;

extern int __VERIFIER_nondet_int(void);

int main()
{
    int x;
    int y;
    x = __VERIFIER_nondet_int();
    y = __VERIFIER_nondet_int();
    // int gh = x+x+y;
    if (x + y <= 0) { 
        while (x > 0) {
            // gh --;
            x = x + x + y;
            y = y - 1;
            // assert (gh >= 0);
        }
    }
    return 0;
}
// I think it's terminated