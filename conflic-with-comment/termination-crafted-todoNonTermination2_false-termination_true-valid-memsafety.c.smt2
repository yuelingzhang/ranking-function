(declare-rel inv (Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var o Int)
(declare-var o1 Int)
(rule 
	(inv x o)
)

(rule (=> 
    (and 
        (inv x o )
        (> x 1)
		(>= x (* 2 o))
		(= x1 o)
    )
    (inv x1 o1 )
  )
)


