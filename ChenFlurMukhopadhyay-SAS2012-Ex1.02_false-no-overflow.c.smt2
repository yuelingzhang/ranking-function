(declare-rel inv (Int Int Int))
(declare-var x Int)
(declare-var x1 Int)
(declare-var oldx Int)
(declare-var oldx1 Int)
(declare-var gh Int)
(declare-var gh1 Int)

(declare-rel err ())

(rule 
  (=> (and (= gh 100)
        (> x 0)
        (< x 100)
      ) 
  (inv x oldx gh))    
)

(rule
  (=> (and (inv x oldx gh)
        (> x 0) 
        (< x 100)
        (>= x (+ (* 2 oldx1 ) 10))
        (= oldx1 x)
        (= gh1 (- gh oldx1 ))
      )
  (inv x1 oldx1 gh1))
)

(rule (=> 
        (and (inv x oldx gh)
          (> x 0) 
          (< x 100)
          (>= x (+ (* 2 oldx ) 10))
          (not 
            (>= gh 0)
          )
        )
        err
      )
)
(query err)